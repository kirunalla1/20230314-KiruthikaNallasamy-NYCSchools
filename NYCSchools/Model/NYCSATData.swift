//
//  NYCSATData.swift
//  NYCSchools
//
//  Created by kiruthika nallasamy on 3/14/23.
//

import Foundation

struct NYCSATData: Codable {
    let dbn: String?
    let numOfTestTakers: String?
    let satReadingScore: String?
    let satMathScore: String?
    let satWritingScore: String?
    
    enum CodingKeys:String, CodingKey {
        
        case numOfTestTakers    = "num_of_sat_test_takers"
        case satReadingScore    = "sat_critical_reading_avg_score"
        case satMathScore       = "sat_math_avg_score"
        case satWritingScore    = "sat_writing_avg_score"
        case dbn
    }
}
