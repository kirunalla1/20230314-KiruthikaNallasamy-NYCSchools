//
//  NYCSchoolData.swift
//  NYCSchools
//
//  Created by kiruthika nallasamy on 3/14/23.
//

import Foundation

struct NYCSchoolData: Codable {
    let dbn: String?
    let schoolName: String?
    let addressLine: String?
    let city: String?
    let finalgrades: String?
    let zipCode: String?
    let totalStudents: String?
    let phoneNumber: String?
    let faxNumber: String?
    let schoolEmail: String?
    let neighborhood: String?
    let overview: String?

    enum CodingKeys: String, CodingKey {
        case schoolName     = "school_name"
        case addressLine    = "primary_address_line_1"
        case zipCode        = "zip"
        case totalStudents  = "total_students"
        case phoneNumber    = "phone_number"
        case faxNumber      = "fax_number"
        case schoolEmail    = "school_email"
        case overview       = "overview_paragraph"
        
        case dbn
        case city
        case finalgrades
        case neighborhood
    }
}

