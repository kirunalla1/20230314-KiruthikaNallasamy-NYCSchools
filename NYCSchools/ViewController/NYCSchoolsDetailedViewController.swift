//
//  NYCSchoolsDetailedViewController.swift
//  NYCSchools
//
//  Created by kiruthika nallasamy on 3/14/23.
//

import Foundation
import UIKit

class NYCSchoolsDetailedViewController: UIViewController {
    
    @IBOutlet weak var schoolNameLabel      : UILabel?
    @IBOutlet weak var addressLabel         : UILabel?
    @IBOutlet weak var cityLabel            : UILabel?
    @IBOutlet weak var zipcodeLabel         : UILabel?
    @IBOutlet weak var phoneLabel           : UILabel?
    @IBOutlet weak var faxLabel             : UILabel?
    @IBOutlet weak var emailLabel           : UILabel?
    @IBOutlet weak var gradesLabel          : UILabel?
    @IBOutlet weak var neighboorLabel       : UILabel?
    @IBOutlet weak var totalStudentsLabel   : UILabel?
    @IBOutlet weak var overviewLabel        : UILabel?
    @IBOutlet weak var satTestTakersLabel   : UILabel?
    @IBOutlet weak var satReadingScoreLabel : UILabel?
    @IBOutlet weak var satWritingScoreLabel : UILabel?
    @IBOutlet weak var satMathScoreLabel    : UILabel?

    var schoolDetails: (schoolData: NYCSchoolData?, satData: NYCSATData?)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSchoolDetails(schoolDetails.schoolData, schoolDetails.satData)
    }
}

extension NYCSchoolsDetailedViewController {
    
    private func setSchoolDetails(_ schoolData: NYCSchoolData?, _ satData: NYCSATData?) {
        
        if let schoolData = schoolData {
            schoolNameLabel?.text = schoolData.schoolName
            addressLabel?.text = schoolData.addressLine
            cityLabel?.text = schoolData.city
            zipcodeLabel?.text = schoolData.zipCode
            phoneLabel?.text = schoolData.phoneNumber
            faxLabel?.text = schoolData.faxNumber
            emailLabel?.text = schoolData.schoolEmail
            gradesLabel?.text = schoolData.finalgrades
            neighboorLabel?.text = schoolData.neighborhood
            totalStudentsLabel?.text = schoolData.totalStudents
            overviewLabel?.text = schoolData.overview
        }
        
        if let satData = satData {
            satTestTakersLabel?.text = satData.numOfTestTakers
            satReadingScoreLabel?.text = satData.satReadingScore
            satWritingScoreLabel?.text = satData.satWritingScore
            satMathScoreLabel?.text = satData.satMathScore
        }
    }
}
