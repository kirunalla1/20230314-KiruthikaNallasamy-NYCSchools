//
//  NYCSchoolsViewController.swift
//  v
//
//  Created by kiruthika nallasamy on 3/14/23.
//

/* I would like to add features like pull down to refresh, search schools by school name etc when get a time.*/

import UIKit

class NYCSchoolsViewController: UIViewController {
    
    @IBOutlet weak var errorLabel: UILabel?
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView?
    @IBOutlet weak var schoolsTableView: UITableView?
    
    private var schoolsViewModel: NYCSchoolsViewModel?
    private var satViewModel: NYCSATViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSchoolsTableView()
        loadSchoolDetails()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailedViewController = segue.destination as? NYCSchoolsDetailedViewController,
            let sender = sender as? NYCSchoolsTableViewCell,
            let schoolsData = schoolsViewModel?.getSchoolsData() {
            let schoolData = schoolsData[sender.tag]
            if let schoolDbn = schoolData.dbn {
                let satData = satViewModel?.getSATDataForSchool(dbn: schoolDbn)
                detailedViewController.schoolDetails = (schoolData: schoolData, satData: satData)
            }
        }
    }
}

extension NYCSchoolsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolsViewModel?.getSchoolsData()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NYCSchoolsTableViewCellId", for: indexPath) as! NYCSchoolsTableViewCell
        
        if let schoolsData = schoolsViewModel?.getSchoolsData() {
            let schoolData = schoolsData[indexPath.row]
            cell.tag = indexPath.row
            cell.schoolNameLabel?.text = schoolData.schoolName
            cell.cityLabel?.text = schoolData.city
            cell.gradeLabel?.text = "Grades: " + (schoolData.finalgrades ?? "NA")
        }
        return cell
    }
}

extension NYCSchoolsViewController {
    
    private func setupSchoolsTableView() {
        schoolsTableView?.rowHeight = 110.0
        schoolsTableView?.backgroundColor = UIColor.clear
        schoolsTableView?.separatorColor = UIColor.white
        schoolsTableView?.dataSource = self
    }
    
    private func loadSchoolDetails() {
        activityIndicatorView?.startAnimating()
        Task {
            schoolsViewModel = NYCSchoolsViewModel()
            if let schoolsViewModel = schoolsViewModel {
                await schoolsViewModel.fetchSchoolData()
            }
            
            satViewModel = NYCSATViewModel()
            if let satViewModel = satViewModel {
                await satViewModel.fetchSchoolSATData()
            }
            
            DispatchQueue.main.async {
                self.activityIndicatorView?.stopAnimating()
                let schoolsData = self.schoolsViewModel?.getSchoolsData()
                if schoolsData == nil || schoolsData?.count == 0 {
                    self.errorLabel?.isHidden = false
                    self.schoolsTableView?.isHidden = true
                }
                else {
                    self.errorLabel?.isHidden = true
                    self.schoolsTableView?.isHidden = false
                    self.schoolsTableView?.reloadData()
                }
            }
        }
    }
}
