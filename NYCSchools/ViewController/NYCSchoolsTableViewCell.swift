//
//  NYCSchoolsTableViewCell.swift
//  NYCSchools
//
//  Created by kiruthika nallasamy on 3/15/23.
//

import Foundation
import UIKit

class NYCSchoolsTableViewCell: UITableViewCell {
    @IBOutlet weak var schoolNameLabel  : UILabel?
    @IBOutlet weak var cityLabel        : UILabel?
    @IBOutlet weak var gradeLabel       : UILabel?
}
