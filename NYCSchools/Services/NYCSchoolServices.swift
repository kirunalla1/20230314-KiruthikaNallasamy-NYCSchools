//
//  NYCNetworkServices.swift
//  NYCSchools
//
//  Created by kiruthika nallasamy on 3/14/23.
//

/*NYCNetworkServices - Services class to send requet and receive data from / to server. async/await pattern used to send and receive data asynchronously in concurrent environment */

import Foundation

class NYCSchoolServices {
    
    static let baseUrl = "https://data.cityofnewyork.us/resource/"
    static let schoolsEndPoint = "s3k6-pzi2.json"
    static let satEndPoint = "f9bf-2cp4.json"
    
    static let shared = NYCSchoolServices()

    func getSchoolsData() async -> [NYCSchoolData]? {
        let url = NYCSchoolServices.baseUrl + NYCSchoolServices.schoolsEndPoint
        do {
            if let data = try await sendRequest(url: url) {
                let schoolModel = try JSONDecoder().decode([NYCSchoolData].self, from: data)
                return schoolModel
            }
        } catch {
            print("getSchoolsData() - Exception")
        }
        return nil
    }
    
    func getSATData() async-> [NYCSATData]? {
        let url = NYCSchoolServices.baseUrl + NYCSchoolServices.satEndPoint
        do {
            if let data = try await sendRequest(url: url) {
                let satData = try JSONDecoder().decode([NYCSATData].self, from: data)
                return satData
            }
        } catch {
            print("getSATData() - Exception")
        }
        return nil
    }
    
    func sendRequest(url: String)async throws -> Data? {
        if let url = URL(string: url) {
            let (data, _) = try await URLSession.shared.data(from: url)
            return data
        }
        return nil
    }
}
