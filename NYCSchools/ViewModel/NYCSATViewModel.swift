//
//  NYCSATViewModel.swift
//  NYCSchools
//
//  Created by kiruthika nallasamy on 3/15/23.
//

import Foundation

//NYCSATViewModel - This class can fetch schools sat details from the server, asynchronously using async / await. Store the sat data and allows the view controller to pull the details and present it in the view on demand basis.

class NYCSATViewModel {
    private var schoolsSATData: [NYCSATData]?
    
    func fetchSchoolSATData() async {
        schoolsSATData = await NYCSchoolServices.shared.getSATData()
    }
    
    func getSATDataForSchool(dbn: String)-> NYCSATData? {
        return schoolsSATData?.filter{ return $0.dbn == dbn }.first
    }
}
