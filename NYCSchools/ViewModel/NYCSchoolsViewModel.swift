//
//  NYCSchoolsViewModel.swift
//  NYCSchools
//
//  Created by kiruthika nallasamy on 3/15/23.
//

//NYCSchoolsViewModel - This class can fetch schools details from the server asynchronously using async / await

import Foundation

class NYCSchoolsViewModel {
    private var schoolsData: [NYCSchoolData]?
    
    func fetchSchoolData() async {
        schoolsData = await NYCSchoolServices.shared.getSchoolsData()
    }
    
    func getSchoolsData()-> [NYCSchoolData]? {
        return schoolsData
    }
}
