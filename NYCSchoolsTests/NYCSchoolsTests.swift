//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by kiruthika nallasamy on 3/15/23.
//

import XCTest
@testable import _0230314_KiruthikaNallasamy_NYCSchools

class NYCSchoolsTests: XCTestCase {
    
    var sutSchoolsViewModel: NYCSchoolsViewModel!
    var sutSATViewModel: NYCSATViewModel!

    override func setUpWithError() throws {
        sutSchoolsViewModel = NYCSchoolsViewModel()
        sutSATViewModel = NYCSATViewModel()
    }

    override func tearDownWithError() throws {
        sutSchoolsViewModel = nil
        sutSATViewModel = nil
    }

    func testSchoolsViewModel() async throws {
        await sutSchoolsViewModel.fetchSchoolData()
        let schoolsData = sutSchoolsViewModel.getSchoolsData()
        XCTAssert(schoolsData != nil && schoolsData?.count != 0)
    }
    
    func testSATViewModel() async throws {
        await sutSATViewModel.fetchSchoolSATData()
        let satData = sutSATViewModel.getSATDataForSchool(dbn: "22K405")
        XCTAssert(satData != nil && satData?.numOfTestTakers != nil)
    }


}
