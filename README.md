# 20230314-KiruthikaNallasamy-NYCSchools



The NYC Schools demo app uses API calls to obtain information about NYC schools from the NYC DOE. The MVVM architecture is used in this app. The MVVM aims to create modular code that is simple to maintain, unit test, and prevent UIKit dependencies during unit tests. By including all data retrieve and data format logic within the ViewModel, it lightens the load on the view controllers. This project has a minimal amount of unit testing to show how the XC Unit testing framework is used for ViewModel, Data Service unit test. Async/Await pattern is used for sending and receving data asynchronously in concurrent environment.

Use below link to download source from gitlab via HTTPS https://gitlab.com/kirunalla1/20230314-KiruthikaNallasamy-NYCSchools.git

**Features to add when get time**
1. Pull down to refresh 
2. Search option to search schools by name
